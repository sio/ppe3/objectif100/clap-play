#include <iostream>
#include <libpq-fe.h>
#include <syslog.h>
#include <string>
#include <gflags/gflags.h>
#include "../header/playlist.h"
#include "../header/clap_play.h"
#include "../header/album.h"
#include "../header/file_configuration.h"
#include "../header/log.h"

int main(int nombre_de_parametres, char *tableau_des_parametres[])
{
  Log logger(false, "main.cc");
  logger.log_debug("BEGIN");
  configure();
  gflags::ParseCommandLineFlags(&nombre_de_parametres, &tableau_des_parametres, true);
  FileConfiguration *config_file = new FileConfiguration();
  config_file->load(FLAGS_config_file);
  std::string db_host = FLAGS_db_host;
  std::string db_name = FLAGS_db_name;
  std::string db_user = FLAGS_db_user;
  std::string db_password = FLAGS_db_password;
  std::string title = FLAGS_title;
  unsigned int duration = FLAGS_duration;
  bool m3u = FLAGS_m3u;
  bool xspf = FLAGS_xspf;
  std::string genre = FLAGS_genre;
  std::string name_playlist = FLAGS_playlist;
  unsigned int polyphony = FLAGS_polyphony;
  std::string format = FLAGS_format;
  std::string artist = FLAGS_artist;
  std::string export_file = FLAGS_export_file;
  
  if(config_file->getConfigLoaded())
  {
    db_host = config_file->getDbHost();
    db_name = config_file->getDbName();
    db_user = config_file->getDbUser();
    db_password = config_file->getDbPassword();
    title = config_file->getTitle();
    duration = config_file->getDuration();
    m3u = config_file->getM3u();
    xspf = config_file->getXspf();
    genre = config_file->getGenre();
    name_playlist = config_file->getNamePlaylist();
  }

  PGconn *retour_connexion = PQsetdbLogin(db_host.c_str(), "5432", nullptr, nullptr, db_name.c_str(), db_user.c_str(), db_password.c_str());

  if(PQstatus(retour_connexion) != CONNECTION_OK)
  {
    logger.log_fatal("Database connection failed : " + db_host);
  }
  else
  {
    logger.log_info("Database connection success");

    if(xspf && m3u)
    {
      Playlist *playlist = new Playlist(3, name_playlist, duration, export_file, retour_connexion);
      playlist->writeXSPF(title, genre, format, polyphony, artist);
      playlist->writeM3U(title, genre, format, polyphony, artist);
    }
    else if(xspf)
    {
      Playlist *playlist = new Playlist(1, name_playlist, duration, export_file, retour_connexion);
      playlist->writeXSPF(title, genre, format, polyphony, artist);
    }
    else
    {
      Playlist *playlist = new Playlist(2, name_playlist, duration, export_file, retour_connexion);
      playlist->writeM3U(title, genre, format, polyphony, artist);
    }
  }

  gflags::ShutDownCommandLineFlags();
  logger.log_debug("END");
  return 0;
}
