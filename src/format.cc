#include "../header/format.h"
#include <iostream>
#include <stdlib.h>

Format::Format(unsigned int id_track, PGconn* connexion){
  std::string req = "SELECT \"format\".titule FROM \"format\" INNER JOIN \"morceau\" on \"morceau\".id = \"format\".id WHERE \"morceau\".id = $1";
  const char *val[] = {std::to_string(id_track).c_str()};
  const Oid types[] = {20};
  PGresult *result_req = PQexecParams(connexion,
                                     req.c_str(),
                                     1,
                                     types,
                                     val,
                                     NULL,
                                     NULL,
                                     0);
  id = atoi(PQgetvalue(result_req, 0, 0));
  titled = PQgetvalue(result_req, 0 , 1);
}

unsigned int Format::getId(){
  return id;
}

std::string Format::getTitled(){
  return titled;
}
