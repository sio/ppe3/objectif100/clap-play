#ifndef M3U_H
#define M3U_H

#include <string>
#include <libpq-fe.h>

/**
 * \class M3U
 *
 * \brief Class allowing the creation of a M3U extended playlist file
 */
class M3U {

private:
  unsigned int id;
  std::string name;
  unsigned int duration;
  std::string export_option;
  PGconn *connexion;
    
  void createFile();
  void addToFile(std::string, std::string, std::string, unsigned int);  
  
public:

  /**
   * \brief Unique constructor used to create an empty M3U extended file
   *
   * \param unsigned integer - id : id of the m3u file
   *
   * \param std::string - name : name of the m3u file
   *
   * \param unsigned integer - duration : duration of the playlist in seconds
   *
   * \param  PGconn * - connexion : handler for the database connexion
   */
  M3U(unsigned int, std::string, unsigned int, std::string, PGconn *);

 /**
  * Destructor
  */
  ~M3U();
  
/**
 * \brief Get the duration of the m3u playlist
 *
 * \return duration : the duartion of the playlist as an unsigned integer
 */
  unsigned int getDuration();

  /**
   * \brief Retrieve a track from the database and write into the file
   *
   * \param unsigned integer - id : the id of the track to add in the playlist
   */
  void addTrack(unsigned int);  
};

#endif //M3U_H
