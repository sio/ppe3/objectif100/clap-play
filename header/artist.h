#ifndef ARTIST_H
#define ARTIST_H

#include <iostream>
#include <libpq-fe.h>

/**
 * \class Artist
 *
 * \brief Allow to retrieve the attributes of the database
 */
class Artist{
private:
  unsigned int id;
  std::string name;
public:

/**
 * \brief Unique constructor which retrieve the value of the attributes from the database according to a track
 *
 * \param unsigned int - id_track : id of the track related to the artist
 *
 * \param PGconn * - connexion : handler for the database connexion
 */
  Artist(unsigned int, PGconn *);

 /**
  * \brief Get the id of the artist
  *
  * \return id : the id of the artist as an unsigned int
  */
  unsigned int getId();

  /**
   * \brief Get the name of the artist
   * 
   * \return name : the name of the artist as a std::string
   */
  std::string getName();
};

#endif // ARTIST_H
